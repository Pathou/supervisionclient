<?php
namespace Carbone\SupervisionClient;

/**
 * Description of Supervision
 *
 * @author pdevalois
 */
class SupervisionClient
{
  /**
   * Url de l'application de supervision
   * @var string
   */
  private $url;
  
  /**
   * Clé de connexion
   * @var string 
   */
  private $key;
  
  /**
   * Client Rest
   * @var type 
   */
  private $client;
  
  /**
   * @param string $url
   * @param string $key
   * @throws ConnectionException
   */
  public function __construct($url, $key)
  {
    $this->url = $url;
    $this->key = $key;
    
    try {
      $this->client = $this->_connexion();
    }
    catch(\Exception $e) {
      throw new ConnectionException('Impossible Connection');
    }
  }

  /**
   * Retourne une taille formaté depuis une taille octet
   * @param int $bytes
   * @param int $decimals
   * @param bool $put_suffix
   * @return type
   */
  static public function humanFilesize($bytes, $decimals = 2, $put_suffix = true) {
    $sz = ['o','Ko','Mo','Go','To','Po'];
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . ($put_suffix ? @$sz[$factor] : '');
  }

  /**
   * Retourne une taille formaté depuis
   * @param int $bytes
   * @param string $toUnit
   * @param int $decimals
   * @return type
   */
  static public function toUnit($bytes, $toUnit, $decimals = 2) {
    $sz = ['o' => 0,'Ko' => 1,'Mo' => 2,'Go' => 3,'To' => 4,'Po' => 5];
    $u = isset($sz[$toUnit]) ? $sz[$toUnit] : $sz[0];
    $value = ($bytes/pow(1024,floor($u)));
    return sprintf("%.{$decimals}f", $value);
  }

  /**
   * Retourne toutess les infos
   * @return bool Ping Réussi ou non
   */
  public function all() {
    
    return $this->_execute(function($client) {
      
      $ret = json_decode($client->get('all', ['key' => $this->key])->getContent());
      $ret->ping->latency = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
      
      return $ret;
      
    });
    
  }
  
  /**
   * Ping le serveur
   * @return bool Ping Réussi ou non
   */
  public function ping() {
    
    return $this->_execute(function($client) {
      
      return json_decode($client->get('ping', ['key' => $this->key])->getContent());
      
    });
    
  }
  
  /**
   * Test le temps de latence
   * @return float Temps de lantence en Microsenconde
   */
  public function latency() {
    
    return $this->_execute(function($client) {

      $res = $client->get('ping', ['key' => $this->key])->getContent();
      return ['latency' => microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"], 'build_time' => json_decode($res, true)['time']];
      
    });
    
  }
  
  /**
   * Retourne les infos du serveur
   * @param string $dist_key
   * @return array [uptime, idletime]
   */
  public function infos() {
    
    return $this->_execute(function($client) {

      return json_decode($client->get('infos', ['key' => $this->key])->getContent());
      
    });
  }
  
  /**
   * Retourne l'espace disque disponible
   * @return array [libre, total]
   */
  public function disk_space() {
    
    return $this->_execute(function($client) {
      
      /*$df = disk_free_space('/');
      $dt = disk_total_space('/');*/
      
      return json_decode($client->get('disk-space', ['key' => $this->key])->getContent());
      
    });
    
  }
  
  /**
   * Retourne le CPU
   * @return array
   */
  public function cpu() {
    
    return $this->_execute(function($client) {
      
      /*
        function get_server_cpu_usage(){

            $load = sys_getloadavg();
            return $load[0];

        }
       */
      
      return json_decode($client->get('cpu', ['key' => $this->key])->getContent());
      
    });
    
  }
  
  /**
   * Retourne la Ram
   * @return array
   */
  public function ram() {
    
    return $this->_execute(function($client) {
      
      /*
        function getSystemMemInfo() 
        {       
            $data = explode("\n", file_get_contents("/proc/meminfo"));
            $meminfo = array();
            foreach ($data as $line) {
              list($key, $val) = explode(":", $line);
              $meminfo[$key] = trim($val);
            }
            return $meminfo;
        }
       */
      
      return json_decode($client->get('ram', ['key' => $this->key])->getContent());
      
    });
    
  }
  
  /**
   * Retourne l'uptime
   * @return bool Ping Réussi ou non
   */
  public function uptime() {
    
    return $this->_execute(function($client) {
      
      return json_decode($client->get('uptime', ['key' => $this->key])->getContent());
      
    });
    
  }
  
  /**
   * Retourne une connexion de db
   * @return boolean
   */
  public function test_db() {
    
    return $this->_execute(function($client) {
      
      return json_decode($client->get('test_db', ['key' => $this->key])->getContent());
      
    });
    
  }
  
  /**
   * Retourne un dump de db
   * @return boolean
   */
  public function dump_db() {
    
    return $this->_execute(function($client) {
      
      return json_decode($client->get('dump_db', ['key' => $this->key])->getContent());
      
    });
    
  }
  
  
  /**
   * Lets connect !
   * @return bool Connexion réussie ou non
   */
  private function _connexion() {
    
    $client = new \Carbone\RestClient\RestClient($this->url);
    
    return $client;
  }
  
  /**
   * Execute la fonction
   * @param function $func
   * @return mixed
   * @throws NoClientException
   */
  private function _execute($func) {
    
    if($this->client != null) return $func($this->client);
    else throw new NoClientException('No Client set');
  }
  
}
